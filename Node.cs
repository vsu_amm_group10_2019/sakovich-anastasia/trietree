﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace _4
{
    partial class TrieTreeView
    {
        private class Node : IEnumerable<Node>
        {
            private readonly TreeNode _view;
            private readonly Dictionary<char, Node> _childs;

            public char? Symbol 
            { 
                get => _view.Text != string.Empty
                    ? char.Parse(_view.Text)
                    : null;
            }

            public Node Parent { get; set; }
            public bool EndOfWord { get; set; }

            public Node(char? symbol = null, Node parent = null)
            {
                _childs = new Dictionary<char, Node>();
                _view = new TreeNode(symbol.HasValue
                    ? symbol.Value.ToString()
                    : string.Empty);
                Parent = parent;
            }

            public bool IsLeaf()
                => _childs.Count < 1;

            private void AddNodeToView(Node node)
                => _view.Nodes.Add(node._view);

            private void RemoveNodeFromView(Node node)
                => _view.Nodes.Remove(node._view);

            public Node this[char symbol]
            {
                get
                {
                    if (_childs.ContainsKey(symbol))
                        return _childs[symbol];
                    return null;
                }
                set
                {
                    if (_childs.ContainsKey(symbol))
                    {
                        RemoveNodeFromView(_childs[symbol]);
                        _childs.Remove(symbol);
                    }
                        
                    if (value is not null)
                    {
                        _childs[symbol] = value;
                        AddNodeToView(_childs[symbol]);
                    }
                }
            }

            public IEnumerator<Node> GetEnumerator()
                => _childs.Values.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator()
                => GetEnumerator();

            public static void SetAsRoot(TreeView tree, Node node)
            {
                tree.Nodes.Clear();
                tree.Nodes.Add(node._view);
            }
        }

    }
}
