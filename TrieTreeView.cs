﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace _4
{
    partial class TrieTreeView : ICollection<string>
    {
        private TreeView _view;

        private Node Root { get; set; }

        public int Count { get; protected set; }

        public bool IsReadOnly => false;

        public TrieTreeView(TreeView view)
        {
            _view = view;
            Clear();
        }

        private Node GetLastSymbol(string word)
        {
            if (Root is not null)
            {
                Node current = Root;
                foreach (char symbol in word)
                {
                    if (current[symbol] is not null)
                        current = current[symbol];
                    else
                        return null;
                }
                return current;
            }
            return null;
        }

        private bool RemoveFromNode(Node node)
        {
            if (node.EndOfWord)
            {
                node.EndOfWord = false;
                while (!node.EndOfWord && node.IsLeaf())
                {
                    Node next = node.Parent;

                    foreach (Node child in next)
                        if (child == node)
                            next[child.Symbol.Value] = null;

                    node = next;
                }

                if (node is null)
                    Root = null;

                --Count;

                return true;
            }
            return false;
        }

        private static IEnumerable<string> GetRecursiveWords(Node node, StringBuilder word)
        {
            if (node is not null)
            {
                if (node.EndOfWord)
                    yield return word.ToString();
                foreach (Node child in node)
                {
                    word.Append(child.Symbol.Value);
                    foreach (string result in GetRecursiveWords(child, word))
                        yield return result;
                    word.Remove(word.Length - 1, 1);
                }
            }
        }

        public void Add(string word)
        {
            if (Root is null)
            {
                Root = new Node();
                Node.SetAsRoot(_view, Root);
            }
                
            int index = 0;
            Node current = Root;
            while (index < word.Length)
            {
                char symbol = word[index];
                if (current[symbol] is null)
                    current[symbol] = new Node(symbol, current);
                current = current[symbol];
                ++index;
            }

            current.EndOfWord = true;
            ++Count;
        }

        public void Clear()
        {
            _view.Nodes.Clear();
            Root = null;
            Count = 0;
        }

        public bool Contains(string word)
        {
            Node node = GetLastSymbol(word);
            if (node is not null)
                return node.EndOfWord;
            return false;
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            if (array is null)
                throw new ArgumentNullException(nameof(array));

            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException(nameof(arrayIndex));

            using IEnumerator<string> words = GetEnumerator();
            while (words.MoveNext())
            {
                if (arrayIndex >= array.Length)
                    throw new ArgumentException("The number of elements in the source System.Collections.Generic.ICollection`1 is greater than the available space from arrayIndex to the end of the destination array.");

                array[arrayIndex++] = words.Current;
            }
        }

        public bool Remove(string word)
            => RemoveFromNode(GetLastSymbol(word));

        public virtual object Clone()
        {
            var obj = new TrieTreeView(_view);
            foreach (string word in this)
                obj.Add(word);
            return obj;
        }

        public IEnumerator<string> GetEnumerator()
            => GetRecursiveWords(Root, new StringBuilder()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}
