﻿
namespace _4
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView = new System.Windows.Forms.TreeView();
            this.openTrieButton = new System.Windows.Forms.Button();
            this.letterTextBox = new System.Windows.Forms.TextBox();
            this.findWordsButton = new System.Windows.Forms.Button();
            this.openTrieDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(-1, 1);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(552, 267);
            this.treeView.TabIndex = 0;
            // 
            // openTrieButton
            // 
            this.openTrieButton.Location = new System.Drawing.Point(5, 274);
            this.openTrieButton.Name = "openTrieButton";
            this.openTrieButton.Size = new System.Drawing.Size(132, 23);
            this.openTrieButton.TabIndex = 1;
            this.openTrieButton.Text = "Открыть файл";
            this.openTrieButton.UseVisualStyleBackColor = true;
            this.openTrieButton.Click += new System.EventHandler(this.openTrieButton_Click);
            // 
            // letterTextBox
            // 
            this.letterTextBox.Location = new System.Drawing.Point(143, 274);
            this.letterTextBox.Name = "letterTextBox";
            this.letterTextBox.Size = new System.Drawing.Size(264, 23);
            this.letterTextBox.TabIndex = 2;
            this.letterTextBox.Text = "a";
            this.letterTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.letterTextBox_Validating);
            this.letterTextBox.Validated += new System.EventHandler(this.letterTextBox_Validated);
            // 
            // findWordsButton
            // 
            this.findWordsButton.Location = new System.Drawing.Point(413, 274);
            this.findWordsButton.Name = "findWordsButton";
            this.findWordsButton.Size = new System.Drawing.Size(132, 23);
            this.findWordsButton.TabIndex = 1;
            this.findWordsButton.Text = "Отобразить";
            this.findWordsButton.UseVisualStyleBackColor = true;
            this.findWordsButton.Click += new System.EventHandler(this.findWordsButton_Click);
            // 
            // openTrieDialog
            // 
            this.openTrieDialog.Filter = "Текстовый файл (*.txt)|*.txt|Все файлы|*.*";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 302);
            this.Controls.Add(this.letterTextBox);
            this.Controls.Add(this.findWordsButton);
            this.Controls.Add(this.openTrieButton);
            this.Controls.Add(this.treeView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Button openTrieButton;
        private System.Windows.Forms.TextBox letterTextBox;
        private System.Windows.Forms.Button findWordsButton;
        private System.Windows.Forms.OpenFileDialog openTrieDialog;
    }
}