﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4
{
    public partial class Main : Form
    {
        private TrieTreeView _tree;
        private char _need_symbol;

        public Main()
        {
            InitializeComponent();
            _need_symbol = 'a';
        }

        private void letterTextBox_Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = !char.TryParse(letterTextBox.Text, out var symbol);
            if (e.Cancel)
            {
                letterTextBox.Text = _need_symbol.ToString();
                MessageBox.Show(this, "Значение должно быть символом.", "Произошла ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void letterTextBox_Validated(object sender, EventArgs e)
            => _need_symbol = char.Parse(letterTextBox.Text);

        private void openTrieButton_Click(object sender, EventArgs e)
        {
            if (openTrieDialog.ShowDialog() == DialogResult.OK)
            {
                _tree = new TrieTreeView(treeView);

                using var fs = new FileStream(openTrieDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                using var sr = new StreamReader(fs);

                string line;
                while ((line = sr.ReadLine()) is not null)
                    _tree.Add(line.Trim());
            }
        }

        private void findWordsButton_Click(object sender, EventArgs e)
        {
            var words = _tree.Where(x => x.Contains(_need_symbol)).ToArray();
            foreach (string word in words)
                _tree.Remove(word);
        }
    }
}
